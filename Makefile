MAKE		:=	make --no-print-directory
CC			:=	cc

INCLUDE_DIR	:=	include/
SOURCE_DIR	:=	src/
OBJECT_DIR	:=	.obj/

CFLAGS		:=	-Wall -Wextra -Werror -MD -MP
IFLAGS		:=	-I$(INCLUDE_DIR)
LFLAGS		:=

OBJECTS		:=	$(patsubst %.c,$(OBJECT_DIR)%.o, \
				util/ft_add_vector.c \
				util/ft_at_vector.c \
				util/ft_clear_vector.c \
				util/ft_init_vector.c \
				util/ft_memchr.c \
				util/ft_memcpy.c \
				util/ft_memmove.c \
				util/ft_memset.c \
				util/ft_reallocate_vector.c \
				util/ft_remove_vector.c \
				util/ft_set_alloc_size.c \
				util/ft_strlen.c \
				display_heaps.c \
				get_ai_play.c \
				game.c \
				get_next_line.c \
				main.c \
				parsing.c \
				utils.c )
DEPS 		:=	$(OBJECTS:.o=.d)
NAME		:=	alcu

.PHONY: all clean fclean re

all	:	$(NAME)

-include $(DEPS)

$(NAME)	:$(OBJECTS)
	$(CC) $(CFLAGS) $(IFLAGS) -o $@ $(OBJECTS) $(LFLAGS)

$(OBJECT_DIR):
	mkdir -p $@

$(OBJECT_DIR)%.o:	$(SOURCE_DIR)%.c Makefile | $(OBJECT_DIR)
	@mkdir -p $(shell dirname $@)
	$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<

clean:
	rm -rf $(OBJECT_DIR)

fclean:	clean
	rm -f $(NAME)

re:	fclean
	$(MAKE) all
