#ifndef ALCU_H
# define ALCU_H
# define XK_MISCELLANY
# define XK_LATIN1
# include "ft_vector.h"
# include "get_next_line.h"
# include "mlx.h"
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <X11/keysymdef.h>

enum	e_status
{
	OK,
	NOT_OK = -1
};

typedef struct s_heap
{
	short	amount;
	int		shouldwin;
}	t_heap;

typedef t_vector	t_heaps;

typedef struct s_mlx_img
{
	void			*img;
	unsigned int	*addr;
	int				bits_per_pixel;
	int				line_length;
	int				endian;
}	t_mlx_img;

typedef struct s_mlx
{
	void		*mlx;
	void		*mlx_win;
	int			width;
	int			height;
	t_mlx_img	img;
	int			exit;
	int			lastplay;
}	t_mlx;

typedef struct s_data
{
	t_mlx	mlx;
	t_heaps	heaps;
}	t_data;

size_t	ft_strlen(char *s);
int		init_heaps(t_heaps *heaps, int input_file_fd);
int		take_amount(t_heaps *heaps, char amount);
int		is_digit(int c);
int		putstr(char *str);
int		puterr(char *str);
int		display_heaps(t_heaps *heaps);
int		init_mlx(t_mlx *mlx, int width, int height);
void	destroy_mlx(t_mlx *mlx);
void	display_heaps_mlx(t_mlx *mlx, t_heaps *heaps);
int		ai_play(t_heaps *heaps);
int		game_turn(t_mlx *mlx, t_heaps *heaps, t_vector *backgrounds, int turn);
void	bot_prepare_game(t_heaps *heaps);
int		gameloop(t_mlx *mlx, t_heaps *heaps);
int		load_background_list(t_vector *bglst, t_mlx *mlx, char *list);
int		put_background(t_mlx *mlx, t_vector *bgs, size_t i);
char	*ft_strtok(char *s, char *delim);
char	*ft_strchr(const char *s, int c);

#endif
