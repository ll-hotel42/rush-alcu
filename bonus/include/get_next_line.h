/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccouble <ccouble@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 17:39:50 by ccouble           #+#    #+#             */
/*   Updated: 2024/03/31 04:22:37 by ccouble          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 4096
# endif

# include <sys/types.h>

char	*get_next_line(int fd);

typedef struct s_buffer
{
	char	buf[BUFFER_SIZE];
	ssize_t	start;
	ssize_t	end;
}	t_buffer;

#endif
