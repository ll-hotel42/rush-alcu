#include "alcu.h"

static int	bot_choose_amount(t_heaps *heaps);

int	ai_play(t_heaps *heaps)
{
	int	amount;

	amount = bot_choose_amount(heaps);
	take_amount(heaps, amount);
	if (putstr("AI choose ") == NOT_OK)
		return (NOT_OK);
	amount += '0';
	if (write(1, &amount, 1) == NOT_OK)
		return (NOT_OK);
	if (putstr("\n") == NOT_OK)
		return (NOT_OK);
	return (OK);
}

static int	bot_choose_amount(t_heaps *heaps)
{
	const t_heap	*heap = ft_at_vector(heaps, heaps->size - 1);
	int				optimal;

	if (heap->shouldwin)
		optimal = (heap->amount - 1) % 4;
	else
		optimal = heap->amount % 4;
	// If optimal is equal to zero, that means the game is lost if the other player plays correctly
	// Whatever we play, the position is losing
	if (optimal == 0)
		return (1);
	return (optimal);
}
