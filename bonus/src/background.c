#include "alcu.h"
#include "ft_vector.h"

static void	*load_background(t_mlx *mlx, char *filename);

int	load_background_list(t_vector *bglst, t_mlx *mlx, char *list)
{
	void		*bgimg;
	char		*line;

	ft_init_vector(bglst, sizeof(void *));
	line = ft_strtok(list, ",");
	while (line)
	{
		bgimg = load_background(mlx, line);
		if (bgimg)
		{
			if (ft_add_vector(bglst, &bgimg, 1) == NOT_OK)
			{
				ft_clear_vector(bglst);
				return (NOT_OK);
			}
		}
		else
			puterr("Failed to load a background picture.\n");
		line = ft_strtok(NULL, ",");
	}
	return (OK);
}

static void	*load_background(t_mlx *mlx, char *filename)
{
	int		img_w;
	int		img_h;
	void	*img;

	img = mlx_xpm_file_to_image(mlx->mlx, filename, &img_w, &img_h);
	return (img);
}

int	put_background(t_mlx *mlx, t_vector *bgs, size_t i)
{
	void		**p_img;
	t_mlx_img	img;

	if (i >= bgs->size)
		return (NOT_OK);
	p_img = ft_at_vector(bgs, i);
	img.addr = (unsigned int *) mlx_get_data_addr(*p_img,
			&img.bits_per_pixel, &img.line_length, &img.endian);
	if (mlx->img.bits_per_pixel != sizeof(unsigned int) * 8)
		return (NOT_OK);
	ft_memcpy(mlx->img.addr, img.addr, sizeof(int) * mlx->width * mlx->height);
	return (OK);
}
