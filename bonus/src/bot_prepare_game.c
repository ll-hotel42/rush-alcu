#include "alcu.h"

void	bot_prepare_game(t_heaps *heaps)
{
	size_t	i;
	t_heap	*heap;
	int		needturn;

	i = 0;
	while (i < heaps->size)
	{
		heap = ft_at_vector(heaps, i);
		if (i == 0)
		{
			heap->shouldwin = 1;
			needturn = (heap->amount % 4) != 1;
		}
		else
		{
			if (needturn)
				heap->shouldwin = 1;
			else
				heap->shouldwin = 0;
			needturn = (heap->amount % 4) != heap->shouldwin;
		}
		++i;
	}
}
