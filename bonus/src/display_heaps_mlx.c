#include "alcu.h"
#include "mlx.h"

static void	display_heap(t_mlx *mlx, t_heap *heap, float percent, int count, int stick_width, int stick_height);
static void	display_stick(t_mlx *mlx, int x, int y, int height, int width);
static void	put_pixel(t_mlx *mlx, int x, int y, unsigned int color);
static int	get_longest(t_heaps *heaps);

static int	scale_wh(float wh, int size, int mlx_wh)
{
	if (wh * size > mlx_wh)
		wh = wh * (mlx_wh / (wh * size));
	wh = (int)wh;
	if (wh <= 5)
		wh = 6;
	return ((int)wh);
}

void	display_heaps_mlx(t_mlx *mlx, t_heaps *heaps)
{
	const int	longest = get_longest(heaps);
	const int	stick_width = scale_wh(50, longest, mlx->width);
	const int	stick_height = scale_wh(50, heaps->size, mlx->height);
	t_heap		*heap;
	float		percent;

	for (size_t i = 0; i < heaps->size; i++)
	{
		heap = ft_at_vector(heaps, i);
		if (heaps->size == 1)
			percent = 0.5f;
		else
			percent = (float)i / (heaps->size - 1);
		display_heap(mlx, heap, percent, heaps->size, stick_width, stick_height);
	}
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_win, mlx->img.img, 0, 0);
}

static void	display_heap(t_mlx *mlx, t_heap *heap, float percent, int count, int stick_width, int stick_height)
{
	const int	hgap = (int)(mlx->height / count) * 1.5;
	const int	y = (((mlx->height - hgap) * percent) + (hgap / 2.f) - 20) ;
	const int	xoffset = (mlx->width - (heap->amount * stick_width)) / 2.f;
	int			x;

	x = 0;
	while (x < heap->amount)
	{
		display_stick(mlx, (x * stick_width) + xoffset, y, stick_height - 5, stick_width - 5);
		++x;
	}
}

static void	display_stick(t_mlx *mlx, int x, int y, int height, int width)
{
	int	w;
	int	h;

	h = -1;
	while (++h < height)
	{
		w = -1;
		while (++w < width)
			put_pixel(mlx, x + w, y + h, 0xff0000);
	}
}

static void	put_pixel(t_mlx *mlx, int x, int y, unsigned int color)
{
	if (x < 0 || y < 0 || x >= mlx->width || y >= mlx->height)
		return ;
	*(mlx->img.addr + (y * mlx->width + x)) = color;
}


static int	get_longest(t_heaps *heaps)
{
	size_t	i;
	t_heap	*heap;
	int		longest;

	longest = 0;
	i = 0;
	while (i < heaps->size)
	{
		heap = ft_at_vector(heaps, i);
		if (heap->amount > longest)
			longest = heap->amount;
		++i;
	}
	return (longest);
}
