#include "alcu.h"

int	get_amount(void)
{
	char	*line;
	int 	value;

	putstr("Your move (1-3): ");
	line = get_next_line(STDIN_FILENO);
	while (line)
	{
		if (ft_strlen(line) == 2 && line[1] == '\n' && '1' <= line[0] && line[0] <= '3')
		{
			value = line[0] - '0';
			free(line);
			return (value);
		}
		putstr("Try again (1-3): ");
		free(line);
		line = get_next_line(STDIN_FILENO);
	}
	return (NOT_OK);
}

int	take_amount(t_heaps *heaps, char amount)
{
	t_heap	*heap;

	if (amount == NOT_OK || heaps->size == 0)
		return (NOT_OK);
	heap = ft_at_vector(heaps, heaps->size - 1);
	if (heap->amount <= 3 && amount > heap->amount)
		return (NOT_OK);
	heap->amount -= amount;
	if (heap->amount == 0)
		ft_remove_vector(heaps, heaps->size - 1);
	return (OK);
}

int	game_turn(t_mlx *mlx, t_heaps *heaps, t_vector *backgrounds, int turn)
{
	char amount;

	if (ai_play(heaps) == NOT_OK)
		return (NOT_OK);
	if (heaps->size == 0)
	{
		if (putstr("You won!\n") == NOT_OK)
			return (NOT_OK);
		return (OK);
	}
	put_background(mlx, backgrounds, turn);
	display_heaps_mlx(mlx, heaps);
	while (1)
	{
		amount = get_amount();
		if (amount == NOT_OK)
			return (NOT_OK);
		if (take_amount(heaps, amount) == OK)
			break ;
	}
	if (heaps->size == 0)
	{
		if (putstr("You lose!\n") == NOT_OK)
			return (NOT_OK);
		return (OK);
	}
	return (OK);
}

static void	free_bgs(t_mlx *mlx, t_vector *bgs)
{
	for (size_t i = 0; i < bgs->size; i++)
		mlx_destroy_image(mlx->mlx, *((void **)ft_at_vector(bgs, i)));
	ft_clear_vector(bgs);
}

int	gameloop(t_mlx *mlx, t_heaps *heaps)
{
	char		bgs_path[] = "./resources/fouras0.xpm,./resources/fouras1.xpm,./resources/fouras2.xpm,./resources/fouras3.xpm";
	t_vector	backgrounds;
	t_heap		*heap;
	size_t		turn;
	
	if (load_background_list(&backgrounds, mlx, bgs_path) == NOT_OK)
		return (NOT_OK);
	if (backgrounds.size == 0)
	{
		puterr("Failed to create backround pictures list\n");
		return (NOT_OK);
	}
	heap = ft_at_vector(heaps, heaps->size - 1);
	turn = 0;
	while (heaps->size > 0 && heap->amount > 0)
	{
		if (game_turn(mlx, heaps, &backgrounds, turn) == NOT_OK)
			return (free_bgs(mlx, &backgrounds), NOT_OK);
		if (heaps->size == 0)
			return (free_bgs(mlx, &backgrounds), OK);
		heap = ft_at_vector(heaps, heaps->size - 1);
		turn = (turn + 1) * (turn + 1 < backgrounds.size);
	}
	free_bgs(mlx, &backgrounds);
	return (OK);
}
