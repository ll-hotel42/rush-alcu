#include "alcu.h"
#include "mlx.h"

int	init_mlx(t_mlx *mlx, int width, int height)
{
	ft_memset(mlx, 0, sizeof(t_mlx));
	mlx->mlx = mlx_init();
	if (mlx->mlx == NULL)
		return (NOT_OK);
	mlx->mlx_win = mlx_new_window(mlx->mlx, width, height, "heap game");
	if (mlx->mlx == NULL)
		return (NOT_OK);
	mlx->img.img = mlx_new_image(mlx->mlx, width, height);
	if (mlx->mlx == NULL)
		return (NOT_OK);
	mlx->img.addr = (unsigned int *) mlx_get_data_addr(mlx->img.img,
			&mlx->img.bits_per_pixel, &mlx->img.line_length, &mlx->img.endian);
	if (mlx->img.bits_per_pixel != sizeof(unsigned int) * 8)
		return (NOT_OK);
	mlx->width = width;
	mlx->height = height;
	return (OK);
}
