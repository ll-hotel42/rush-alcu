#include "alcu.h"

static int	get_fd(int argc, char *argv[]);

int	main(int argc, char *argv[])
{
	t_data	data;
	int		fd;
	int		ret;

	fd = get_fd(argc, argv);
	if (fd == NOT_OK)
		return (fd);
	if (init_heaps(&data.heaps, fd) == NOT_OK)
	{
		puterr("ERROR\n");
		return (1);
	}
	if (init_mlx(&data.mlx, 1920, 1080) == NOT_OK)
	{
		destroy_mlx(&data.mlx);
		ft_clear_vector(&data.heaps);
		puterr("ERROR\n");
		return (1);
	}
	if (fd)
		close(fd);
	ret = gameloop(&data.mlx, &data.heaps);
	destroy_mlx(&data.mlx);
	ft_clear_vector(&data.heaps);
	return (ret);
}

static int	get_fd(int argc, char *argv[])
{
	int	fd;

	if (argc > 2)
		return (NOT_OK);
	if (argc == 1)
		fd = 0;
	else
		fd = open(argv[1], O_RDONLY);
	if (fd < 0)
	{
		puterr("Failed to open file\n");
		return (NOT_OK);
	}
	return (fd);
}
