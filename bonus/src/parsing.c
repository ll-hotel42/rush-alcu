/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccouble <ccouble@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/31 23:32:07 by ccouble           #+#    #+#             */
/*   Updated: 2024/03/31 23:48:32 by ccouble          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alcu.h"
#include "ft_vector.h"
#include "get_next_line.h"
#include <errno.h>
#include <limits.h>
#include <stdio.h>

static short	read_value(int fd);
static short	get_number(const char *s);

int	init_heaps(t_heaps *heaps, int input_file_fd)
{
	t_heap	heap;

	ft_init_vector(heaps, sizeof(t_heap));
	heap.amount = read_value(input_file_fd);
	heap.shouldwin = 0;
	while (heap.amount > 0)
	{
		if (heap.amount > 10000)
			return (NOT_OK);
		if (ft_add_vector(heaps, &heap, 1) == -1)
		{
			ft_clear_vector(heaps);
			return (NOT_OK);
		}
		heap.amount = read_value(input_file_fd);
	}
	if (heap.amount == NOT_OK)
	{
		ft_clear_vector(heaps);
		return (NOT_OK);
	}
	if (heaps->size == 0)
		return (NOT_OK);
	bot_prepare_game(heaps);
	return (OK);
}

static short	read_value(int fd)
{
	short	value;
	char	*line;

	line = get_next_line(fd);
	if (line == NULL)
	{
		if (errno == 0)
			return (0);
		return (-1);
	}
	value = get_number(line);
	free(line);
	return (value);
}

static short	get_number(const char *s)
{
	int 	i;
	short	value;

	i = 0;
	value = 0;
	int in = 0;
	while (s[i])
	{
		if (is_digit(s[i]))
		{
			in = 1;
			if (SHRT_MAX / 10 < value)
				return (-1);
			value *= 10;
			if (SHRT_MAX - s[i] - '0' < value)
				return (-1);
			value += s[i] - '0';
		}
		else if (s[i] == '\n')
		{
			if (in && value == 0)
				return (NOT_OK);
			return (value);
		}
		else
			return (NOT_OK);
		++i;
	}
	if (in && value == 0)
		return (NOT_OK);
	return (value);
}
