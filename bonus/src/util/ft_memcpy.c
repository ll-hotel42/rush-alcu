/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccouble <ccouble@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/07 12:56:45 by ccouble           #+#    #+#             */
/*   Updated: 2024/03/31 22:48:24 by ll-hotel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>

void	*ft_memcpy(void *dest, const void *src, uint64_t n)
{
	uint8_t	*d;
	uint8_t	*s;

	d = (uint8_t *)dest;
	s = (uint8_t *)src;
	if (n & 1)
		*(d++) = *(s++);
	if (n & 2)
		*((uint16_t *)d) = *((uint16_t *)s);
	d += n & 2;
	s += n & 2;
	if (n & 4)
		*((uint32_t *)d) = *((uint32_t *)s);
	d += n & 4;
	s += n & 4;
	n >>= 3;
	while (n--)
	{
		*((uint64_t *)d) = *((uint64_t *)s);
		d += 8;
		s += 8;
	}
	return (dest);
}
