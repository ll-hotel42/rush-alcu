/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtok.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccouble <ccouble@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/25 19:14:09 by ccouble           #+#    #+#             */
/*   Updated: 2024/03/31 21:57:41 by ll-hotel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alcu.h"
#include <stddef.h>

static char	*get_next_word(char *s, char *delim);

char	*ft_strtok(char *s, char *delim)
{
	static char	*str = NULL;
	char		*rvalue;

	if (s)
	{
		str = s;
		while (*str && ft_strchr(delim, *str))
			++str;
		if (*str == '\0')
			str = NULL;
	}
	if (str)
	{
		rvalue = str;
		str = get_next_word(str, delim);
		return (rvalue);
	}
	return (NULL);
}

static char	*get_next_word(char *s, char *delim)
{
	while (*s && ft_strchr(delim, *s) == NULL)
		++s;
	while (*s && ft_strchr(delim, *s))
	{
		*s = '\0';
		++s;
	}
	if (*s == '\0')
		s = NULL;
	return (s);
}
