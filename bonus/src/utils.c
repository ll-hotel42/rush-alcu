#include "alcu.h"

int	putstr(char *str)
{
	long	i;

	for (i = 0; str[i]; i++);
	return (write(1, str, i));
}

int	puterr(char *str)
{
	long	i;

	for (i = 0; str[i]; i++);
	return (write(2, str, i));
}

int	is_digit(int c)
{
	return ('0' <= c && c <= '9');
}
