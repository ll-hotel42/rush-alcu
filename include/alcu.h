#ifndef ALCU_H
# define ALCU_H
# include "ft_vector.h"
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>

enum	e_num
{
	OK,
	NOT_OK = -1,
	ZERO = '0',
	ONE = '1',
	TWO = '2',
	THREE = '3'
};

typedef struct s_heap
{
	short	amount;
	int		shouldwin;
}	t_heap;

typedef t_vector	t_heaps;

size_t	ft_strlen(char *s);
int		init_heaps(t_heaps *heaps, int input_file_fd);
int		take_amount(t_heaps *heaps, char amount);
void	*ft_realloc(void *p, long old_size, long new_size);
int		ft_atoi(const char *nptr);
int		gameloop(t_heaps *heaps);
int		is_digit(int c);
int		putstr(char *str);
int		puterr(char *str);
int		display_heaps(t_heaps *heaps);
int		get_ai_play(t_heaps *heaps);

#endif
