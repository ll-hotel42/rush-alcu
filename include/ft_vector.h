/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccouble <ccouble@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 09:13:30 by ccouble           #+#    #+#             */
/*   Updated: 2024/03/31 05:50:50 by ccouble          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_VECTOR_H
# define FT_VECTOR_H

# include <stddef.h>

# define BASE_ALLOC 16

typedef struct s_vector
{
	size_t	elemsize;
	size_t	size;
	size_t	allocated;
	void	*array;

}	t_vector;

void	*ft_memcpy(void *dest, const void *src, size_t n);
void	*ft_memmove(void *dest, const void *src, size_t n);
void	*ft_memset(void *s, int c, size_t n);
void	*ft_memchr(const void *s, int c, size_t n);
int		ft_add_vector(t_vector *this, const void *data, size_t count);
int		ft_reallocate_vector(t_vector *this);
void	ft_clear_vector(t_vector *this);
void	ft_remove_vector(t_vector *this, size_t i);
void	ft_init_vector(t_vector *this, size_t elemsize);
void	ft_set_alloc_size(t_vector *this, size_t count);
void	*ft_at_vector(t_vector *this, size_t i);

#endif
