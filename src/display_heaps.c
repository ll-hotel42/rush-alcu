#include "alcu.h"

static int	put_spaces(int amount);
static int	get_longest(t_heaps *heaps);

int	display_heaps(t_heaps *heaps)
{
	const int	longest = get_longest(heaps);
	t_heap		*heap;
	int			first;

	for (size_t i = 0; i < heaps->size; i++)
	{
		heap = ft_at_vector(heaps, i);
		first = 1;
		if (put_spaces(longest - heap->amount) == NOT_OK)
			return (NOT_OK);
		for (int j = 0; j < heap->amount; j++)
		{
			if (first)
				first = 0;
			else
			{
				if (putstr(" ") == NOT_OK)
					return (NOT_OK);
			}
			if (putstr("|") == NOT_OK)
				return (NOT_OK);
		}
		if (put_spaces(longest - heap->amount) == NOT_OK)
			return (NOT_OK);
		if (putstr("\n") == NOT_OK)
			return (NOT_OK);
	}
	return (OK);
}

static int	put_spaces(int amount)
{
	for (int i = 0; i < amount; i++)
	{
		if (putstr(" ") == NOT_OK)
			return (NOT_OK);
	}
	return (OK);
}

static int	get_longest(t_heaps *heaps)
{
	size_t	i;
	t_heap	*heap;
	int		longest;

	longest = 0;
	i = 0;
	while (i < heaps->size)
	{
		heap = ft_at_vector(heaps, i);
		if (heap->amount > longest)
			longest = heap->amount;
		++i;
	}
	return (longest);
}
