#include "alcu.h"
#include "get_next_line.h"
#include <stdio.h>

int	get_amount(void)
{
	char	*line;
	int 	value;

	putstr("Your move (1-3): ");
	line = get_next_line(STDIN_FILENO);
	while (line)
	{
		if (ft_strlen(line) == 2 && line[1] == '\n'
			&& '1' <= line[0] && line[0] <= '3')
		{
			value = line[0] - '0';
			free(line);
			return (value);
		}
		putstr("Try again (1-3): ");
		free(line);
		line = get_next_line(STDIN_FILENO);
	}
	return (NOT_OK);
}

int	take_amount(t_heaps *heaps, char amount)
{
	t_heap	*heap;

	if (amount == NOT_OK || heaps->size == 0)
		return (NOT_OK);
	heap = ft_at_vector(heaps, heaps->size - 1);
	if (heap->amount <= 3 && amount > heap->amount)
		return (NOT_OK);
	heap->amount -= amount;
	if (heap->amount == 0)
		ft_remove_vector(heaps, heaps->size - 1);
	return (OK);
}

int	gameloop(t_heaps *heaps)
{
	char	amount;
	t_heap	*heap;
	
	heap = ft_at_vector(heaps, heaps->size - 1);
	while (heaps->size > 0 && heap->amount > 0)
	{
		if (display_heaps(heaps) == NOT_OK)
			return (NOT_OK);
		amount = get_ai_play(heaps);
		take_amount(heaps, amount);
		if (putstr("AI choose ") == NOT_OK)
			return (NOT_OK);
		amount += '0';
		if (write(1, &amount, 1) == NOT_OK)
			return (NOT_OK);
		if (putstr("\n") == NOT_OK)
			return (NOT_OK);
		if (heaps->size == 0)
		{
			if (putstr("You won!") == NOT_OK)
				return (NOT_OK);
			return (OK);
		}
		if (display_heaps(heaps) == NOT_OK)
			return (NOT_OK);
		while (1)
		{
			amount = get_amount();
			if (amount == NOT_OK)
				return (NOT_OK);
			if (take_amount(heaps, amount) == OK)
				break ;
		}
		if (heaps->size == 0)
		{
			if (putstr("You lose!") == NOT_OK)
				return (NOT_OK);
			return (OK);
		}
		heap = ft_at_vector(heaps, heaps->size - 1);
	}
	return (OK);
}
