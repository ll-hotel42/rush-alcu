#include "alcu.h"

int	get_ai_play(t_heaps *heaps)
{
	const t_heap	*heap = ft_at_vector(heaps, heaps->size - 1);
	int				optimal;

	if (heap->shouldwin)
		optimal = (heap->amount - 1) % 4;
	else
		optimal = heap->amount % 4;
	if (optimal == 0)
		return (1);
	return (optimal);
}
