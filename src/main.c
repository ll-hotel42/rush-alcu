#include "alcu.h"

int	main(int argc, const char *argv[])
{
	t_heaps	heaps;
	int		input_file_fd;
	int		ret;

	if (argc > 2)
		return (1);
	if (argc == 1)
		input_file_fd = 0;
	else
		input_file_fd = open(argv[1], O_RDONLY);
	if (input_file_fd < 0)
	{
		puterr("Failed to open file\n");
		return (1);
	}
	if (init_heaps(&heaps, input_file_fd) == NOT_OK)
	{
		puterr("ERROR\n");
		return (1);
	}
	if (input_file_fd)
		close(input_file_fd);
	ret = gameloop(&heaps);
	ft_clear_vector(&heaps);
	return (ret);
}
